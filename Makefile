###
docker/registry/login:
	$$(aws ecr get-login --no-include-email --region us-east-1)

CHART_NAME=$(shell cat chart/spin-helm-demo/Chart.yaml | grep name | awk '{print $2}')
CHART_VERSION=$(shell cat chart/spin-helm-demo/Chart.yaml | grep version | awk '{print $2}')

APP_VERSION ?= $(shell cat chart/spin-helm-demo/Chart.yaml | grep appVersion| awk '{print $2}')

CHART_BUCKET ?= f5cs-spinnaker-demo
DOCKER_REPO ?= spin-helm-repo
SPINNAKER_API ?= http://a3711e3628c5211e9807402ca3ffbb8f-400776773.us-east-1.elb.amazonaws.com:8084

docker:
	docker build -t $(DOCKER_REPO):$(APP_VERSION) .
	docker tag $(DOCKER_REPO):$(APP_VERSION) $(DOCKER_REPO):latest

dockerpush: docker
	docker push $(DOCKER_REPO):$(APP_VERSION)
	docker push $(DOCKER_REPO):latest

compile:
	helm package chart/spin-helm-demo

upload:
	aws s3 cp $(CHART_NAME)-$(CHART_VERSION).tgz s3://$(CHART_BUCKET)/packages/
	aws s3 cp values/dev.yaml s3://$(CHART_BUCKET)/packagevalues/$(CHART_NAME)/dev.yaml
	aws s3 cp values/prod.yaml s3://$(CHART_BUCKET)/packagevalues/$(CHART_NAME)/prod.yaml

triggerdocker:
	curl -L -vvv -X POST \
		-k \
		-H"Content-Type: application/json" $(SPINNAKER_API)/webhooks/webhook/spinhelmdemo \
		-d '{"artifacts": [{"type": "docker/image", "name": "$(CHART_NAME)", "reference": "$(DOCKER_REPO):$(APP_VERSION)", "kind": "docker"}]}'

triggerchart:
	curl -L -vvv -X POST \
		-k \
		-H"Content-Type: application/json" $(SPINNAKER_API)/webhooks/webhook/spinhelmdemo \
		-d '{"artifacts": [{"type": "s3/object", "name": "s3://$(CHART_BUCKET)/packages/spin-helm-demo-0.1.0.tgz", "reference": "s3://$(CHART_BUCKET)/packages/spin-helm-demo-$(CHART_VERSION).tgz", "kind": "s3"}]}'
